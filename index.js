import express from 'express'
import pg_client from 'pg'
import { resolve } from 'path'

import dotenv from 'dotenv'
dotenv.config();

const app = express()
const port = 3000

app.use(express.static('.'))

app.get('/contacts', (req, res) => {
  res.sendFile(resolve('index.html'))
})

app.get('/data', (req, res) => {
  let client = new pg_client.Client({
    user: process.env.PGUSER,
    host: process.env.PGHOST,
    database: process.env.PGDATABASE,
    password: process.env.PGPASSWORD,
    port: process.env.PGPORT,
  })
  client.connect()

  client.query('select * from contacts, phones where phones.contact_id = contacts.id;', (err, result) => {
    if (err) {
      console.log(err.stack)
    }
    res.send(result.rows.slice(0, 100))

    client.end()
  })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
