# Merci Edgar Bis

Refaire Merci Edgar avec un outillage plus léger qu'une application rails.
C'est peut-être plus simple que d'essayer de reprendre l'ancienne et 
la mettre à jour.

## Développement

Utilisation de [Svelte](https://svelte.dev/) en partant d'[un template Svelte par David Bruant](https://github.com/DavidBruant/front-end-template).

Pour installer, il faut faire `npm install` et `cp .env.sample .env`

    Pour le moment, le moment, le fichier .env contient
    les informations de connexion à la base de donnée

Ensuite, il y a deux choses à lancer `npm start` et `npm run dev:rollup`

