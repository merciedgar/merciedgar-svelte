import App from './App.svelte';
import Contacts from './Contacts.svelte';
import { json } from 'd3-fetch';

import page from 'page'

let currentComponent

page(`/contacts`,context => {

  json("/data").then(
    data => {
      console.log("dans contacts, then ", data)
      if (currentComponent) {
        currentComponent.$destroy()
      }
      currentComponent = new Contacts({
        target: document.querySelector('.svelte-main'),
        props: { data }
      });
    }
  )
})

page('/', context => {
  if (currentComponent) {
    currentComponent.$destroy()
  }
  currentComponent = new App({
    target: document.querySelector('.svelte-main'),
    props: { }
  })
})

page.start()
